package com.example.assignment.features.common

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactive.asFlow
import org.springframework.data.domain.PageImpl
import org.springframework.util.StringUtils
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyAndAwait
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.json
import reactor.core.publisher.Mono


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
data class ApiResponse<out T>(
    val code: String,
    val message: String,

    val pageNo: Int? = null,
    val totalPage: Int? = null,
    val last: Boolean? = null,

    val result: T? = null,
)


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
data class ApiPageResponse<out T>(
    val code: String,
    val message: String,

    val pageNo: Int? = null,
    val totalPage: Int? = null,
    val last: Boolean? = null,

    val result: Flow<T>? = null
)


object ApiCode {
    const val CODE_SUCCESS200 = "SUCCESS200";
    const val CODE_DATA_NOT_FOUND = "ERROR404";
    const val CODE_ERROR = "ERROR500";
    const val CODE_ERROR_409 = "ERROR409";

    const val SUCCESS200 = "SUCCESS200";
    const val DUPLICATE_DATA = "409";
    const val MESSAGE_DATA_SAME = "ชื่อจริง และนามสกุลนี้ เป็นสมาชิกแล้ว"

    const val MESSAGE_SUCCESS = "SUCCESS";
    const val MESSAGE_ERROR = "ERROR";
    const val MESSAGE_DATA_NOT_FOUND = "Data Not Found";

}

//suspend inline fun <T> error(): ApiResponse<T> = error(ApiCode.MESSAGE_ERROR)
suspend inline fun <T> apiDeleteSuccess(
    response: T? = null,
) =
    ServerResponse
        .ok()
        .json()
        .bodyValueAndAwait(
            ApiResponse(
                code = ApiCode.CODE_SUCCESS200,
                message = "DELETE SUCCESS",
                result = null
            )
        )

suspend inline fun <T> apiError(
    response: T? = null,
    msg: String
) =
    ServerResponse
        .ok()
        .json()
        .bodyValueAndAwait(
            ApiResponse(
                code = ApiCode.CODE_ERROR_409,
                message = msg,
                result = null
            )
        )

suspend inline fun <T> apiSuccess(
    response: T? = null,
    pageNo: Int? = null,
    totalPage: Int? = null,
    last: Boolean? = null
) =
    ServerResponse
        .ok()
        .json()
        .bodyValueAndAwait(
            ApiResponse(
                code = ApiCode.CODE_SUCCESS200,
                message = ApiCode.MESSAGE_SUCCESS,
                result = response,
                pageNo = pageNo,
                totalPage = totalPage,
                last = last
            )
        )

suspend inline fun <T> apiResponse(
    apiCode : String,
    message : String,
    result: T?,
    response: T? = null,
    pageNo: Int? = null,
    totalPage: Int? = null,
    last: Boolean? = null
) =
    ServerResponse
        .ok()
        .json()
        .bodyValueAndAwait(
            ApiResponse(
                code = apiCode,
                message = message,
                result = result,
                pageNo = pageNo,
                totalPage = totalPage,
                last = last
            )
        )

suspend inline fun <T> apiSuccessfully(
    response: T? = null,
    pageNo: Int? = null,
    totalPage: Int? = null,
    last: Boolean? = null
) =
    ServerResponse
        .ok()
        .json()
        .bodyValueAndAwait(
            ApiResponse(
                code = ApiCode.CODE_ERROR_409,
                message = "ชื่อจริง และนามสกุลนี้ เป็นสมาชิกแล้ว",
                result = response,
                pageNo = pageNo,
                totalPage = totalPage,
                last = last
            )
        )

suspend inline fun <E, T> apiPageSuccess(
    response: Mono<PageImpl<E>>,
    crossinline transform: (entity: E) -> T
) =
    ServerResponse
        .ok()
        .json()
        .bodyAndAwait(
            response.map { page ->
                ApiResponse(
                    code = if (page.totalElements > 0) ApiCode.CODE_SUCCESS200 else ApiCode.CODE_DATA_NOT_FOUND,
                    message = if (page.totalElements > 0) ApiCode.MESSAGE_SUCCESS else ApiCode.MESSAGE_DATA_NOT_FOUND,
                    result = page.content.map {
                        transform(it)
                    },
                    pageNo = page.number,
                    totalPage = page.totalPages,
                    last = page.isLast
                )
            }.asFlow()
        )

suspend inline fun <T> SUCCESS(
    response: T? = null,
    pageNo: Int? = null,
    totalPage: Int? = null,
    last: Boolean? = null
) =
    ServerResponse
        .ok()
        .json()
        .bodyValueAndAwait(
            ApiResponse(
                code = if (response != null) ApiCode.CODE_SUCCESS200 else ApiCode.CODE_DATA_NOT_FOUND,
                message = if (response != null) ApiCode.MESSAGE_SUCCESS else ApiCode.MESSAGE_DATA_NOT_FOUND,
                result = response
            )
        )

