package com.example.assignment.features.member.register

import com.example.assignment.data.entities.member.MemberRegisterEntities
import com.example.assignment.features.common.*
import com.example.assignment.features.member.register.models.CreateMemberRegisterRequest
import com.example.assignment.features.member.register.models.MemberRegisterResponse
import com.example.assignment.features.member.register.services.MemberRegisterService
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.withContext
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.awaitBody

@Component
class MemberRegisterHandler(
    private val memberRegisterService: MemberRegisterService,
) {
    suspend fun register(request: ServerRequest) = withContext(NonCancellable) {
        val body: CreateMemberRegisterRequest = request.awaitBody()
        val exits = memberRegisterService.findPathFullNameAndLastName(body.firstName, body.lastName)
        val result = if (exits == null) memberRegisterService.register(body) else null
        apiResponse(
            apiCode = if (result == null) ApiCode.DUPLICATE_DATA else ApiCode.SUCCESS200,
            message = if (result == null) ApiCode.MESSAGE_DATA_SAME else ApiCode.MESSAGE_SUCCESS,
            result = result?.transformData()
        )
    }

    @FlowPreview
    suspend fun list(request: ServerRequest) = withContext(NonCancellable) {
        apiPageSuccess(memberRegisterService.list(0), transform = { it.transformData() })
    }

    suspend fun findInFoMemberId(request: ServerRequest) = withContext(NonCancellable) {
        val foundPost = request.pathVariable("memberId").toLong()
        apiSuccess(
            response = memberRegisterService.findPathId(foundPost)?.transformData()
        )
    }

    suspend fun deleteById(request: ServerRequest) = withContext(NonCancellable) {
        apiDeleteSuccess(
            response = memberRegisterService.deleteById(request.awaitBody()).transformData()
        )
    }

    suspend fun updateById(request: ServerRequest) = withContext(NonCancellable) {
        apiSuccess(
            response = memberRegisterService.update(request.awaitBody())?.transformData()
        )
    }

    fun MemberRegisterEntities.transformData() = MemberRegisterResponse(
        memberId = this.memberId ?: 0,
        titleName = this.titleName ?: "",
        firstName = this.firstName ?: "",
        lastName = this.lastName ?: "",
        mobileNo = this.mobileNo ?: ""
    )
}