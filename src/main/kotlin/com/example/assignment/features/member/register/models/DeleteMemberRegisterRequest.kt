package com.example.assignment.features.member.register.models

data class DeleteMemberRegisterRequest(
    val memberId: Long
)