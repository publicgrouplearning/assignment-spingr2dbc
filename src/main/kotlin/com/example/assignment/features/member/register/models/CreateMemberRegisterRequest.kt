package com.example.assignment.features.member.register.models

data class CreateMemberRegisterRequest(
    val memberId: Long? = null,
    val titleName: String,
    val firstName: String,
    val lastName: String,
    val mobileNo: String

//    Request Example don't require fill citizen_no
)