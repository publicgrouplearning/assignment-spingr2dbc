package com.example.assignment.features.member.register

import kotlinx.coroutines.FlowPreview
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.reactive.function.server.coRouter

@Configuration
class MemberRegisterRouter {

    @FlowPreview
    @Bean
    fun routerMemberRegister(handler: MemberRegisterHandler) = coRouter {
        accept(MediaType.APPLICATION_JSON).nest {
            "/members".nest {
                POST("/register", handler::register)
                GET("/list", handler::list)
                POST("/deleteById", handler::deleteById)
                GET("/info/{memberId}", handler::findInFoMemberId)
                POST("/update", handler::updateById)
            }
        }
    }
}

