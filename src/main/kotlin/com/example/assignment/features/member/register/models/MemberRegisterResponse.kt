package com.example.assignment.features.member.register.models

data class MemberRegisterResponse(
    val memberId: Long,
    val titleName: String,
    val firstName: String,
    val lastName: String,
    val mobileNo: String

)

