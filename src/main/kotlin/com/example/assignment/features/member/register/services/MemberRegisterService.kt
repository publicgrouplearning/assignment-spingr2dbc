package com.example.assignment.features.member.register.services

import com.example.assignment.data.entities.member.MemberRegisterEntities
import com.example.assignment.data.usecase.member.register.*
import com.example.assignment.features.common.ApiCode
import com.example.assignment.features.common.apiDeleteSuccess
import com.example.assignment.features.member.register.models.*
import org.springframework.stereotype.Service

@Service
class MemberRegisterService(
    private val saveMemberRegisterUseCase: SaveMemberRegisterUseCase,
    private val listMemberRegisterUseCase: ListMemberRegisterUseCase,
    private val deleteMemberRegisterUseCase: DeleteMemberRegisterUseCase,
    private val getMemberRegisterUseCase: GetMemberRegisterUseCase,
    private val updateMemberRegisterUseCase: UpdateMemberRegisterUseCase
) {

    suspend fun register(request: CreateMemberRegisterRequest): MemberRegisterEntities? {
        val findFullName = findFullName(request)
        return if (findFullName != null) {
            request.memberId?.let { getMemberRegisterUseCase.get(it) }
        } else {
            MemberRegisterEntities(
                titleName = request.titleName,
                firstName = request.firstName,
                lastName = request.lastName,
                mobileNo = request.mobileNo
            ).also {
                saveMemberRegisterUseCase.save(it)
            }
        }
    }

    suspend fun update(request: CreateMemberRegisterRequest): MemberRegisterEntities? {
        return request.let {
            MemberRegisterEntities(
                memberId = request.memberId,
                titleName = request.titleName,
                firstName = request.firstName,
                lastName = request.lastName,
                mobileNo = request.mobileNo
            ).also {
                updateMemberRegisterUseCase.update(it)
            }
        }
    }

    suspend fun list(pageNo: Int, pageSize: Int = 20) =
        listMemberRegisterUseCase.list(pageNo = pageNo, pageSize = pageSize)

    suspend fun findPathId(memberId: Long) =
        getMemberRegisterUseCase.get(memberId)

    suspend fun findPathFullNameAndLastName(firstName: String, lastName: String) =
        getMemberRegisterUseCase.findByMyName(firstName, lastName)

    suspend fun deleteById(request: DeleteMemberRegisterRequest): MemberRegisterEntities =
        request.let {
            MemberRegisterEntities(
                memberId = it.memberId
            ).also { entities ->
                deleteMemberRegisterUseCase.deleteById(entities)
            }
        }

    suspend fun findFullName(request: CreateMemberRegisterRequest): MemberRegisterEntities? =
        getMemberRegisterUseCase.findByMyName(request.firstName, request.lastName)

    suspend fun findFullNameAndLastName(
        request: CreateMemberRegisterRequest? = null,
        firstName: String,
        lastName: String
    ) {
        request?.let { getMemberRegisterUseCase.findByMyName(it.firstName, request.lastName) }

    }

    private fun MemberRegisterEntities.transformData() = MemberRegisterResponse(
        memberId = memberId ?: 0,
        titleName = titleName ?: "",
        firstName = firstName ?: "",
        lastName = lastName ?: "",
        mobileNo = mobileNo ?: ""
    )
}