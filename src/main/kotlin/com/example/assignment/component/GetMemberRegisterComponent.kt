package com.example.assignment.component

import com.example.assignment.data.repositories.member.MemberRegisterRepository
import com.example.assignment.exception.MessageMapperException
import com.example.assignment.utils.ErrorCode.USER_PROFILE_NOT_FOUND

class GetMemberRegisterComponent(
    private val memberRegisterRepository: MemberRegisterRepository
) {

    private suspend fun getMemberRegisterInCache(memberId: Long) =
        memberRegisterRepository.findById(memberId)

    private suspend fun getUserProfileByIdentify(memberId: Long) =
        memberRegisterRepository.findById(memberId)
            ?: throw MessageMapperException(
                USER_PROFILE_NOT_FOUND,
                "Not Found User Profile identifier: $memberId"
            )
}