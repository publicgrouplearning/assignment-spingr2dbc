package com.example.assignment.exception


data class MessageMapperException(val statusCd: String, val statusDesc: String? = null) : RuntimeException()