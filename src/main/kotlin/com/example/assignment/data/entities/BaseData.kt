package com.example.assignment.data.entities

import java.time.LocalDateTime

open class BaseData {
    var isDeleted: String = "N"
    var createdBy: String = "System"
    var createdDate: LocalDateTime = LocalDateTime.now()
    var updatedBy: String = "System"
    var updatedDate: LocalDateTime = LocalDateTime.now()
}