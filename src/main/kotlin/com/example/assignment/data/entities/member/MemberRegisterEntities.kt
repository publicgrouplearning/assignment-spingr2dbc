package com.example.assignment.data.entities.member

import com.example.assignment.data.entities.BaseData
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("MS_MEMBER_REGISTER")
data class MemberRegisterEntities(
    @Id var memberId: Long? = null,
    var titleName: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var mobileNo: String? = null,
    var citizenNo: String? = null
) : BaseData()