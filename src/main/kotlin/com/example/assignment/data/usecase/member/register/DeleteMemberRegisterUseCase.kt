package com.example.assignment.data.usecase.member.register

import com.example.assignment.data.entities.member.MemberRegisterEntities
import com.example.assignment.data.repositories.member.MemberRegisterRepository
import org.springframework.stereotype.Component

@Component
class DeleteMemberRegisterUseCase(
    private val memberRegisterRepository: MemberRegisterRepository
) {
    suspend fun deleteById(memberFriend: MemberRegisterEntities) =
        memberFriend.memberId?.let {
            memberRegisterRepository.deleteById(it)
        }
}