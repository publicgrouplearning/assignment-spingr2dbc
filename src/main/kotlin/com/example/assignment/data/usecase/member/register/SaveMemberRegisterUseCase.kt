package com.example.assignment.data.usecase.member.register

import com.example.assignment.data.entities.member.MemberRegisterEntities
import com.example.assignment.data.repositories.member.MemberRegisterRepository
import org.springframework.stereotype.Component

@Component
class SaveMemberRegisterUseCase(
    private val memberRegisterRepository: MemberRegisterRepository
) {
    suspend fun save(memberRegisterEntities: MemberRegisterEntities) =
        memberRegisterRepository.save(memberRegisterEntities)
}