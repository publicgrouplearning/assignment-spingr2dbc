package com.example.assignment.data.usecase.member.register

import com.example.assignment.data.entities.member.MemberRegisterEntities
import com.example.assignment.data.repositories.member.MemberRegisterRepository
import org.springframework.stereotype.Component

@Component
class UpdateMemberRegisterUseCase(
    private val memberRegisterRepository: MemberRegisterRepository
) {
    suspend fun update(memberRegisterEntities: MemberRegisterEntities) =
        memberRegisterRepository.update(memberRegisterEntities)

}