package com.example.assignment.data.usecase.member.register

import com.example.assignment.data.entities.member.MemberRegisterEntities
import com.example.assignment.data.repositories.member.MemberRegisterRepository
import org.springframework.stereotype.Component

@Component
class ListMemberRegisterUseCase(
    private val memberRegisterRepository: MemberRegisterRepository
) {
    suspend fun list(pageNo: Int = 0, pageSize: Int = 20) =
        memberRegisterRepository.list<MemberRegisterEntities>(pageNo = pageNo, pageSize = pageSize)

    suspend fun listMemberId(memberId: Long = 0) =
        memberRegisterRepository.list<MemberRegisterEntities>()
}

