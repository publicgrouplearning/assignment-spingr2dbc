package com.example.assignment.data.usecase.member.register

import com.example.assignment.data.repositories.member.MemberRegisterRepository
import org.springframework.stereotype.Component

@Component
class GetMemberRegisterUseCase(
    private val memberRegisterRepository: MemberRegisterRepository
) {
    suspend fun get(memberId: Long) = memberRegisterRepository.findById(memberId)

    suspend fun findByMyName(firstName: String, lastName: String) =
        memberRegisterRepository.findByMyName(firstName, lastName)

}
