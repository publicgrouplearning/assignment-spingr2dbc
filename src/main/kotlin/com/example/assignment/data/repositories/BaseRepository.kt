package com.example.assignment.data.repositories

import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate
import org.springframework.data.relational.core.query.Criteria
import org.springframework.data.relational.core.query.Query
import reactor.core.publisher.Mono


open class BaseRepository<E : Any>(
    open val databaseClient: DatabaseClient,
    open val r2dbcEntityTemplate: R2dbcEntityTemplate
) {
    fun page(pageNo: Int = 0, pageSize: Int = 20) = PageRequest.of(pageNo, pageSize)

    inline fun <reified T : E> list(pageNo: Int = 0, pageSize: Int = 20): Mono<PageImpl<T>> {
        val pageable = page(pageNo = pageNo, pageSize = pageSize)
        val query = Query
            .query(Criteria.where("is_deleted").`is`("N"))
            .with(pageable)
        return findAll(query = query, pageable = pageable)
    }

    inline fun <reified T : E> findAll(query: Query, pageable: Pageable): Mono<PageImpl<T>> {
        val countValue = r2dbcEntityTemplate.count(query, T::class.java)
        val list = r2dbcEntityTemplate.select(query, T::class.java)
        return Mono.zip(countValue, list.buffer().next().defaultIfEmpty(emptyList()))
            .map { output ->
                PageImpl(
                    output.t2,
                    pageable,
                    output.t1
                )
            }
    }

    suspend inline fun <reified T : E> save(entity: T) = r2dbcEntityTemplate.insert(entity).awaitFirstOrNull()
//        databaseClient.insert()
//            .into(T::class.java)
//            .using(entity)
//            .fetch()
//            .awaitRowsUpdated()
//            .run {
//                println("111111111 $this")
//            }
//    }

    suspend inline fun <reified T : E> update(entity: T) = r2dbcEntityTemplate.update(entity).awaitFirstOrNull()
//        databaseClient.update()
//            .table(T::class.java)
//            .using(entity)
//            .fetch()
//            .awaitRowsUpdated()
//            .run {
//                if (this <= 0) {
//                    throw IllegalAccessException("Exception when update ${T::class.java.name}")
//                }
//            }
//    }

}