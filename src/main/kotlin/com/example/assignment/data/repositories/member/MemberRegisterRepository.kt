package com.example.assignment.data.repositories.member

import com.example.assignment.data.entities.member.MemberRegisterEntities
import com.example.assignment.data.repositories.BaseRepository
import kotlinx.coroutines.reactive.awaitSingle
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate
import org.springframework.data.r2dbc.core.awaitFirstOrNull
import org.springframework.data.relational.core.query.Criteria
import org.springframework.stereotype.Repository

@Repository
class MemberRegisterRepository(
    override val databaseClient: DatabaseClient,
    override val r2dbcEntityTemplate: R2dbcEntityTemplate
) : BaseRepository<MemberRegisterEntities>(databaseClient, r2dbcEntityTemplate) {

    suspend fun findById(memberId: Long): MemberRegisterEntities? = databaseClient.select()
        .from(MemberRegisterEntities::class.java)
        .matching(
            Criteria.where("member_id").`is`(memberId)
        ).fetch()
        .awaitFirstOrNull()

    suspend fun findByMyName(firstName: String, LastName: String): MemberRegisterEntities? = databaseClient.select()
        .from(MemberRegisterEntities::class.java)
        .matching(
            Criteria.where("first_name").`is`(firstName).and("last_name").`is`(LastName)
        ).fetch()
        .awaitFirstOrNull()

    suspend fun findByMyAll(
        firstName: String,
        LastName: String,
        mobileNo: String,
        titleName: String
    ): MemberRegisterEntities? = databaseClient.select()
        .from(MemberRegisterEntities::class.java)
        .matching(
            Criteria.where("first_name").`is`(firstName).and("last_name").`is`(LastName)
                .and(mobileNo).`is`("mobile_no").and(titleName).`is`("title_name")
        ).fetch()
        .awaitFirstOrNull()

    suspend fun deleteById(memberId: Long): Int? = databaseClient.delete()
        .from(MemberRegisterEntities::class.java)
        .matching(
            Criteria.where("member_id").`is`(memberId)
        ).fetch()
        .rowsUpdated()
        .awaitSingle()
}