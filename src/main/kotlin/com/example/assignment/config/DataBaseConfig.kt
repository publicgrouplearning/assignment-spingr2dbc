package com.example.assignment.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate


@Configuration
class DatabaseConfig(
    private val databaseClient: DatabaseClient
) {

    @Bean
    fun r2dbcEntityTemplate() = R2dbcEntityTemplate(databaseClient)
}